package com.waleed.simpleProjectSkeleton;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleProjectSkeletonApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleProjectSkeletonApplication.class, args);
	}

}
